public class Tokimon {

    private String name;
    private double size;
    private String ability;
    private int strength;

    public Tokimon(){
        this.name = null;
        this.size = 0.0;
        this.ability = null;
        this.strength = 0;
    }

    @Override
    public String toString() {
        return "Tokimon{" +
                "name='" + name + '\'' +
                ", size=" + size +
                ", ability='" + ability + '\'' +
                ", strength=" + strength +
                '}';
    }

    public void printTokimon(){
        String name = this.getName();
        String ability = this.ability;
        double size = this.getSize();
        int strength = this.getStrength();
        String spaceBetween = " ,";
        System.out.println(name + spaceBetween
                + size + "m" + spaceBetween
                + ability + " ability" + spaceBetween
                + strength + " strength");
    }

    public Tokimon(String name, double size, String ability, int strength) {
        this.name = name;
        this.size = size;
        this.ability = ability;
        this.strength = strength;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public String getName() {
        return name;
    }

    public double getSize() {
        return size;
    }

    public String getAbility() {
        return ability;
    }

    public int getStrength() {
        return strength;
    }

}
