import java.util.ArrayList;
import java.util.Scanner;

public class TokimonTracker {

    private static ArrayList<Tokimon> tokimonsList = new ArrayList<>();
    private static Menu outputMenu = new Menu();

    public static void main(String args[]){

        //print welcome msg to user
        welcomeToTracker();

        //display menu and options
        openMainMenu(outputMenu);
    }

    private static void openMainMenu(Menu outputMenu) {
        //print main menu heading to user
        System.out.println();
        outputMenu.setMenuTitle("Main Menu");
        outputMenu.printMenuHeading();

        //print main menu options to user
        setMenuOptions(outputMenu);
        outputMenu.printMenuOptions();

        //take user input
        int userInput = outputMenu.takeUserInput();

        //based on user input decide the next action
        actionFromUserInput(userInput);
    }


    private static void actionFromUserInput(int userInput) {
        if (userInput == 1){
            listAllTokimons();
        }
        else if (userInput == 2){
            createNewTokimon();
        }
        else if (userInput == 3){
            deleteTokimonFromList();
        }
        else if (userInput == 4){
            changeTokimonStrength();
        }
        else if (userInput == 5){
            displayToStringForAllTokimons();
        }
        else {//userinput=6
            exitTheTracker();
            return;
        }
        openMainMenu(outputMenu);
    }

    private static void exitTheTracker() {
        System.out.println("Bye!");
    }

    private static void displayToStringForAllTokimons() {
        
        for(Tokimon tokimon : tokimonsList){
            System.out.print(tokimonsList.indexOf(tokimon) + ". ");
            System.out.println(tokimon.toString());
            System.out.println();
        }
    }

    private static void changeTokimonStrength() {
        //heading
        menuHeadings("Change Tokimon strength");

        //print all the tokimons
        listAllTokimons();

        //choose a tokimon to change strength
        Scanner userInputUpdateTokimon = new Scanner(System.in);

        System.out.println();
        System.out.println("Select a Tokimon whose strength you wish to update");
        System.out.println("To cancel Press 0");
        System.out.print("> ");

        int updateTokimonIndex = userInputUpdateTokimon.nextInt();

        if(updateTokimonIndex == 0){
            return;
        }

        while (updateTokimonIndex < 0 && updateTokimonIndex > tokimonsList.size()+1){
            System.out.println("Please a valid entry!");
            updateTokimonIndex = userInputUpdateTokimon.nextInt();
        }

        Tokimon updateTokimon = tokimonsList.get(updateTokimonIndex-1);

        System.out.print("Select the new strength for the tokimon:   ");
        int updatedStrength = userInputUpdateTokimon.nextInt();
        System.out.println();
        updateTokimon.setStrength(updatedStrength);
        System.out.println();
        System.out.println("updated");
    }

    private static void deleteTokimonFromList() {
        //heading
        menuHeadings("Delete a Tokimon");


        //print all the tokimons
        listAllTokimons();

        //TODO
        //Refactor this code!!

        //delete a tokimon
        Scanner userInputDeleteTokimon = new Scanner(System.in);

        System.out.println();
        System.out.println("Select a Tokimon you wish to delete:");
        System.out.println("To cancel Press 0");
        System.out.print("> ");

        int deleteTokimonIndex = userInputDeleteTokimon.nextInt();
        if(deleteTokimonIndex == 0){
            return;
        }
        else if(deleteTokimonIndex > 0 || deleteTokimonIndex < tokimonsList.size()){
            tokimonsList.remove(deleteTokimonIndex-1);
        }
        else {
            //don't delete
            //display menu again
            openMainMenu(outputMenu);
        }
    }

    private static void createNewTokimon() {
        //heading
        menuHeadings("Create New Tokimon");

        //create new tokimon
        Tokimon newTokimon = new Tokimon();
        Scanner userTokimonInput = new Scanner(System.in);

        System.out.print("Enter Tokimons Name:   ");
        String name = userTokimonInput.nextLine();
        System.out.println();

        System.out.print("Enter Tokimons Ability:   ");
        String ability = userTokimonInput.nextLine();
        System.out.println();

        System.out.print("Enter Tokimons Strength:   ");
        int strength = userTokimonInput.nextInt();
        System.out.println();

        System.out.print("Enter Tokimons Size:   ");
        double size = userTokimonInput.nextDouble();
        System.out.println();

        newTokimon.setName(name);
        newTokimon.setAbility(ability);
        newTokimon.setStrength(strength);
        newTokimon.setSize(size);

        tokimonsList.add(newTokimon);
    }

    private static void listAllTokimons() {
        //heading
        menuHeadings("List of all Tokimons");

        if(tokimonsList.size() < 1){
            menuHeadings("There are currently no Tokimon's in the list!! Press option 2 to add more.");
            return;
        }

        //list
        for (Tokimon tokimon : tokimonsList){
                System.out.print((tokimonsList.indexOf(tokimon)+1) + ". ");
                tokimon.printTokimon();
        }
    }

    private static void setMenuOptions(Menu optionsMenu) {
        ArrayList<String> options = new ArrayList<>();
        options.add("List Tokimons");
        options.add("Add a new Tokimon");
        options.add("Remove a Tokimon");
        options.add("Change Tokimon strength");
        options.add("DEBUG: Dump objects (toString)");
        options.add("Exit");

        optionsMenu.setMenuOptions(options);
    }


    private static void welcomeToTracker() {
        String welcomeTile = "Tokimon Tracker by Aryan Arora #301325858";
        menuHeadings(welcomeTile);
    }

    private static void menuHeadings(String headingMessage){
        Menu menu = new Menu();
        menu.setMenuTitle(headingMessage);
        menu.printMenuHeading();
    }
}
