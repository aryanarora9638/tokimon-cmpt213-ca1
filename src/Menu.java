import java.util.ArrayList;
import java.util.Scanner;

public class Menu {

    private String menuTitle;
    private ArrayList<String> menuOptions;

    public Menu(){
        this.menuTitle = null;
        this.menuOptions = null;
    }

    public Menu(String menuTitle, ArrayList<String> menuOptions) {
        this.menuTitle = menuTitle;
        this.menuOptions = menuOptions;
    }


    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public ArrayList<String> getMenuOptions() {
        return menuOptions;
    }

    public void setMenuOptions(ArrayList<String> menuOptions) {
        this.menuOptions = menuOptions;
    }

    public void printMenu(){
        //rectangle around menu title
        printMenuHeading();
        // number the options starting from one
        printMenuOptions();

    }

    public void printMenuHeading() {
        String rectangleSide = "*" + " " + this.getMenuTitle() + " " + "*";
        int titleLength = this.getMenuTitle().length();
        int extraLengthOnSides = 4;
        //prints top
        printMenuHeadingTop(titleLength, extraLengthOnSides);

        //prints middle
        System.out.println(rectangleSide);

        //prints bottom
        printMenuHeadingTop(titleLength,extraLengthOnSides);
    }

    private void printMenuHeadingTop(int titleLength, int extraLengthOnSides){
        for (int rectangleSpot = 0 ; rectangleSpot < titleLength + extraLengthOnSides ; rectangleSpot++ ){
            System.out.print("*");
        }
        System.out.println();
    }

    public void printMenuOptions() {
        int optionNumber = 1;
        String optionItem;
        String numberSpace = ". ";
        for (String option: menuOptions) {
            optionItem = String.valueOf(optionNumber) + numberSpace + option;
            System.out.println(optionItem);
            optionNumber++;
        }
    }

    public int takeUserInput(){
        String inputLogo = ">";
        Scanner input = new Scanner(System.in);
        System.out.print(inputLogo);
        int userInput = input.nextInt();
        while (userInput <= 0 || userInput > menuOptions.size()){
            System.out.println("Please enter a valid option!");
            System.out.print(inputLogo);
            userInput = input.nextInt();
        }
        return userInput;
    }
}


//doubt should i take inputs from user only in the classes or just the main function/class
